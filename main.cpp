#include <QIconEnginePlugin>
#include <QDebug>

#include "GFontIconEngine.h"

class GFontIconPlugin : public QIconEnginePlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QIconEngineFactoryInterface" FILE "gfonticonplugin.json")

public:
    QIconEngine *create(const QString &filename = QString()) override;
};

QIconEngine *GFontIconPlugin::create(const QString &filename)
{
    qDebug() << "GFontIconPlugin::create" << filename;
    return new GFontIconEngine;
}

//#include "main.moc"
