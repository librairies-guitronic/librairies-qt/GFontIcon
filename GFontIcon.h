#ifndef GFONTICON_H
#define GFONTICON_H

#include <QColor>
#include <QIcon>
#include "GFontIconLibGlobal.h"

class GFontIcon;

//Creation d'icones
#define GIcon(code)                                         GFontIcon::icon(code)
#define GIconColor(code,color)                              GFontIcon::icon(code,color)
#define GIconFamily(code,fontFamily)                        GFontIcon::icon(code,QColor(),fontFamily)
#define GIconColorFamily(code,color,fontFamily)             GFontIcon::icon(code,color,fontFamily)
//Creation d'icones animés
#define GIconAnim(code,parent)                              GFontIcon::iconAnimed(code,parent)
#define GIconAnimColor(code,color,parent)                   GFontIcon::iconAnimed(code,parent,color)
#define GIconAnimFamily(code,fontFamily,parent)             GFontIcon::iconAnimed(code,parent,QColor(),fontFamily)
#define GIconAnimColorFamily(code,color,fontFamily,parent)  GFontIcon::iconAnimed(code,parent,color,fontFamily)

#ifdef WIN32
	//FontAwesome v5.6
	#define FONT_SOLID_5        "v5/FreeSolid"
	//#define FONT_REGULAR_5      "v5/FreeRegular"
	#define FONT_BRANDS_5       "v5/BrandsRegular"
	//FontAwesome v6.0.0-beta2
	#define FONT_SOLID_6        "v6/FreeSolid"
	//#define FONT_REGULAR_6      "v6/FreeRegular"
	#define FONT_BRANDS_6       "v6/BrandsRegular"
#else
	//FontAwesome v5.6
	#define FONT_SOLID_5        "v5/FreeSolid"
	//#define FONT_REGULAR_5      "v5/FreeSolid"
	#define FONT_BRANDS_5       "v5/BrandsRegular"
	//FontAwesome v6.0.0-beta2
	#define FONT_SOLID_6        "v6/FreeSolid"
	//#define FONT_REGULAR_6      "v6/FreeSolid"
	#define FONT_BRANDS_6       "v6/BrandsRegular"
#endif

/**
 * Initialisation des ressources partagées
 * @brief InitGFontIconRessources
 */
GFONTICON_SHARED_EXPORT void InitGFontIconRessources();

/**
 * Classe principale des fonts Awesome
 *	La seule font chargée par défaut est FONT_FREE
 * @brief The GFontIcon class
 */
class GFONTICON_SHARED_EXPORT GFontIcon
{

	public:
		/**
		 * Type d'animations possibles
		 * @brief The TypeAnimation enum
		 */
		enum TypeAnimation
		{
			AnimRotation,
			AnimClignote
		};

		/**
		 * Singleton de GFontIcon
		 * @brief instance
		 * @return
		 */
		static GFontIcon    instance();

		/**
		 * Icone simple en pixmap issu de GFontIconeEngine pour redimenssionement
		 * @see https://fontawesome.com/v5/search?o=r&s=solid
		 * @brief icon
		 * @param code
		 * @param baseColor
		 * @param family
		 * @return
		 */
		static QIcon icon(const int &code, const QColor &baseColor = QColor(), const QString &family = FONT_SOLID_5);
		static QIcon icon(const QChar &code, const QColor &baseColor = QColor(), const QString &family = FONT_SOLID_5);

		/**
		 * Icone double en bitmap
		 * @brief iconOnOff
		 * @param taille
		 * @param codeOn
		 * @param codeOff
		 * @param baseColorOn
		 * @param baseColorOff
		 * @param family
		 * @return
		 */
		static QIcon iconOnOff(int taille, const int &codeOn, const int &codeOff, const QColor &baseColorOn = QColor(), const QColor &baseColorOff = QColor(), const QString &family = FONT_SOLID_5);
		static QIcon iconOnOff(int taille, const QChar &codeOn, const QChar &codeOff, const QColor &baseColorOn = QColor(), const QColor &baseColorOff = QColor(), const QString &family = FONT_SOLID_5);

		/**
		 * Icone issu de GFontIconeEngine
		 * @brief iconAnimed
		 * @param code
		 * @param parent
		 * @param baseColor
		 * @param family
		 * @param frequence
		 * @return
		 */
		static QIcon iconAnimed(const int &code, QObject *parent, const QColor& baseColor = QColor(), TypeAnimation typeAnimation = AnimRotation, const QString& family = FONT_SOLID_5, const qreal &frequence = 1.);
		static QIcon iconAnimed(const QChar& code, QObject *parent, const QColor& baseColor = QColor(), TypeAnimation typeAnimation = AnimRotation, const QString& family = FONT_SOLID_5, const qreal &frequence = 1.);

		/**
		 * Retourne la famille de police chargée
		 *  S'assure du bon chargement des polices awesome
		 * @brief Family
		 * @param family
		 * @return
		 */
		static QString  Family(const QString &family);

	private:
		GFontIcon();

		/**
		 * Liste des polices en cours
		 * @brief _fontFamilies
		 */
		static QHash<QString, QString>  _FontFamilies;

		/**
		 * Ajout d'une font
		 * @brief _addFamily
		 * @param family
		 */
		static bool     _AddFamily(const QString& family);
};

#endif // GFONTICON_H
