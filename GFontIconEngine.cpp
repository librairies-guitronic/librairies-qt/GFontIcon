#include "GFontIconEngine.h"

#include <QApplication>
#include <QPalette>
#include <QStyle>
#include <QPixmapCache>

const QPixmap GFontIconEngine::Pixmap(const QChar &code, const QColor &baseColor, const QFont &font, const QSize &size, QIcon::Mode mode, QIcon::State state)
{
	QPixmap pix(size);
	pix.fill(Qt::transparent);
	//Dessine le pixmap
	QPainter painter(&pix);
	Paint(code, baseColor, font, &painter, QRect(QPoint(0,0),size), mode, state);
	return pix;
}

void GFontIconEngine::Paint(const QChar &code, const QColor &baseColor, const QFont &font, QPainter *painter, const QRect &rect, QIcon::Mode mode, QIcon::State state)
{
	Q_UNUSED(state);

	const QRect rectReduit = rect.adjusted(1,1, -1,-1);
	//Adapte la font à la taille du dessin
	QFont fontDraw(font);
	fontDraw.setPixelSize( qRound( qreal(rect.height()) * .8) );
	//fontDraw.setPixelSize( rectReduit.height() );
	painter->setFont(fontDraw);

	//Couleur de l'icone en fonction de l'état
	QColor penColor;
	if (mode == QIcon::Disabled)
	{
		penColor = QApplication::palette("QWidget").color(QPalette::Disabled, QPalette::ButtonText);
	}
	else if (mode == QIcon::Selected)
	{
		penColor = QApplication::palette("QWidget").color(QPalette::Active, QPalette::ButtonText);
	}
	else if (baseColor.isValid())
	{
		penColor = baseColor;
	}
	else
	{
		penColor = QApplication::palette("QWidget").buttonText().color();
	}

	//Parametrage du painter
	painter->setPen(penColor);

	//Dessin par le style
	QApplication::style()->drawItemText(painter, rectReduit, Qt::AlignCenter, QApplication::palette("QWidget"),
										(mode != QIcon::Disabled), code);
}




GFontIconEngine::GFontIconEngine()
	: GFontIconEngine("", QChar(0xf000))
{ }

GFontIconEngine::GFontIconEngine(const QString &family, const QChar &letter, const QColor &baseColor)
	: QIconEngine(),
	  _letter(letter),
	  _baseColor(baseColor),
	  _font(family)
{
	_font.setStyleStrategy(QFont::PreferAntialias);

	//qDebug() << "GFontIconEngine" << _font.families() << _font.styleName();
}

void GFontIconEngine::paint(QPainter *painter, const QRect &rect, QIcon::Mode mode, QIcon::State state)
{
	GFontIconEngine::Paint(_letter, _baseColor, _font, painter, rect, mode, state);
}


QPixmap GFontIconEngine::pixmap(const QSize &size, QIcon::Mode mode, QIcon::State state)
{
	return GFontIconEngine::Pixmap(_letter, _baseColor, _font, size, mode, state);

	//Avec prise en charge du cache
    /*QPixmap pm;
	const QString key = QString("%1_%2_%3-%4").arg(_font.family()).arg(_letter).arg(size.width()).arg(size.height());
	if (!QPixmapCache::find(key, &pm))
	{
		pm = GFontIconEngine::Pixmap(_letter, _baseColor, _font, size, mode, state);
		QPixmapCache::insert(key, pm);
	}
    return pm;*/
}

QIconEngine *GFontIconEngine::clone() const
{
	return new GFontIconEngine(_font.family(), _letter, _baseColor);
}

QString GFontIconEngine::key() const
{
	return "GFontIconEngine";
}

QString GFontIconEngine::iconName()
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
	const
#endif
{
	return "GFontIconEngine";
}

QList<QSize> GFontIconEngine::availableSizes(QIcon::Mode mode, QIcon::State state)
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
	const
#endif
{
	return QIconEngine::availableSizes(mode, state);
}

void GFontIconEngine::virtual_hook(int id, void *data)
{
    switch(id)
	{
        case QIconEngine::IsNullHook:
        {
			*reinterpret_cast<bool*>(data) = false;
            return;
        }
		case QIconEngine::ScaledPixmapHook:
        {
            ScaledPixmapArgument *info = reinterpret_cast<ScaledPixmapArgument*>(data);
            info->pixmap = GFontIconEngine::Pixmap(_letter, _baseColor, _font, info->size, info->mode, info->state);
			return;
		}
		default:
			break;
	}
    //qDebug() << "GFontIconEngine::virtual_hook" << id << data;
	QIconEngine::virtual_hook(id, data);
}
