#include "GFontIcon.h"

#include <QFontDatabase>
#include "GFontAnimIconEngine.h"


void InitGFontIconRessources()
{
	Q_INIT_RESOURCE(resource_v5);
	Q_INIT_RESOURCE(resource_v6);
}

//Déclaration de static _FontFamilies
QHash<QString, QString> GFontIcon::_FontFamilies = QHash<QString, QString>();

GFontIcon GFontIcon::instance() {
	static GFontIcon _fontIconInstance;
	return _fontIconInstance;
}

QIcon GFontIcon::icon(const int &code, const QColor &baseColor, const QString &family)
{
	return icon(QChar(code), baseColor, family);
}

QIcon GFontIcon::icon(const QChar &code, const QColor &baseColor, const QString &family)
{
	return QIcon(new GFontIconEngine( GFontIcon::Family(family), code, baseColor));
}

QIcon GFontIcon::iconOnOff(int taille, const int &codeOn, const int &codeOff, const QColor &baseColorOn, const QColor &baseColorOff, const QString &family)
{
	return iconOnOff(taille, QChar(codeOn), QChar(codeOff), baseColorOn, baseColorOff, family);
}

QIcon GFontIcon::iconOnOff(int taille, const QChar &codeOn, const QChar &codeOff, const QColor &baseColorOn, const QColor &baseColorOff, const QString &family)
{
	QIcon iconOnOff;
	iconOnOff.addPixmap(GFontIcon::icon(codeOn, baseColorOn, family).pixmap(taille, taille), QIcon::Normal, QIcon::On);
	iconOnOff.addPixmap(GFontIcon::icon(codeOff, baseColorOff, family).pixmap(taille, taille), QIcon::Normal, QIcon::Off);
	return iconOnOff;
}

QIcon GFontIcon::iconAnimed(const int &code, QObject *parent, const QColor &baseColor, TypeAnimation typeAnimation, const QString &family, const qreal &frequence)
{
	return iconAnimed(QChar(code), parent, baseColor, typeAnimation, family, frequence);
}

QIcon GFontIcon::iconAnimed(const QChar &code, QObject *parent, const QColor &baseColor, TypeAnimation typeAnimation, const QString &family, const qreal &frequence)
{
	return QIcon(new GFontAnimIconEngine(frequence, code, baseColor, typeAnimation, GFontIcon::Family(family), parent));
}

QString GFontIcon::Family(const QString &family)
{
	//Charge la police
	if( _FontFamilies.contains(family) ||
		_AddFamily(family) )
	{
		return _FontFamilies.value(family);
	}
	return "";
}




GFontIcon::GFontIcon()
{ }


bool GFontIcon::_AddFamily(const QString &family)
{
	const int id = QFontDatabase::addApplicationFont(QString(":/fonts/%1").arg(family));
	//qDebug() << "_AddFamily" << family << id << QFontDatabase::applicationFontFamilies(id) << _FontFamilies;
	if( id > -1 &&
		!QFontDatabase::applicationFontFamilies(id).isEmpty() )
	{
		_FontFamilies.insert(family, QFontDatabase::applicationFontFamilies(id).constFirst());
		return true;
	}
	qWarning() << QObject::tr("Impossible de charger la famille de police") << family;
	return false;
}
