#-------------------------------------------------
#
# Fichiers sources de QFontIcon
#
#-------------------------------------------------
#Inclusion du path
INCLUDEPATH += $$PWD

HEADERS += $$PWD/GFontIcon.h \
    $$PWD/GFontIconEngine.h \
    $$PWD/GFontAnimIconEngine.h \
    $$PWD/GFontIconLibGlobal.h

SOURCES += \
    $$PWD/GFontIcon.cpp \
    $$PWD/GFontIconEngine.cpp \
    $$PWD/GFontAnimIconEngine.cpp

RESOURCES += \
    $$PWD/resource_v5.qrc \
    $$PWD/resource_v6.qrc
