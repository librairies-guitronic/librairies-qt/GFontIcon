#-------------------------------------------------
#
# Project created by QtCreator 2017-01-03T13:37:08
#
#-------------------------------------------------
QT += widgets

#Compilation statique
#CONFIG += static
#QMAKE_CXXFLAGS += -static -static-libgcc -static-libstdc++ -lstdc++ -lpthread
#QMAKE_LFLAGS += -static

TARGET = GFontIcon
DEFINES += GFONTICON_LIBRARY

# Version programme
VER_MAJ = 1
VER_MIN = 0
VER_PAT = 6
VER_BUILD = 0
VERSION = $${VER_MAJ}.$${VER_MIN}.$${VER_PAT}.$${VER_BUILD}

QMAKE_TARGET_PRODUCT = "Librairie FontIcon Guitronic pour Qt"$$QT_VERSION
QMAKE_TARGET_DESCRIPTION = "Iconise les fontawesome 5.6.3 et 6 beta"

# Complete informations Guitronic par default
include("$$PWD/../GlobalLibGuitronic.pro")

# Inclusion des fichiers
include("$$PWD/GFontIconInclude.pri")

#A voir
#CONFIG     += plugin
#PLUGIN_TYPE = iconengines
#PLUGIN_CLASS_NAME = GFontIconPlugin
#QTC_PLUGIN_NAME = GFontIconPlugin
#OTHER_FILES += gfonticonplugin.json
#load(qt_plugin)
