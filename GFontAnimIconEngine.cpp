#include "GFontAnimIconEngine.h"

#include <QTimerEvent>
#include <cmath>
#include <QAbstractButton>
#include <QMenu>
#include <QLabel>

GFontAnimIconEngine::GFontAnimIconEngine(QObject *parent)
	: GFontAnimIconEngine(1, ' ', QColor(), GFontIcon::AnimRotation, "", parent)
{}

GFontAnimIconEngine::GFontAnimIconEngine(const qreal &frequence, const QChar &letter, const QColor &baseColor, const GFontIcon::TypeAnimation &typeAnimation, const QString &family, QObject *parent)
	: QObject(),
	  GFontIconEngine(family, letter, baseColor),
	  _typeAnimation(typeAnimation),
	  _frequence(frequence),
	  _parent(parent)
{
}

GFontAnimIconEngine::~GFontAnimIconEngine()
{
	_stopTimer();
}

void GFontAnimIconEngine::paint(QPainter *painter, const QRect &rect, QIcon::Mode mode, QIcon::State state)
{
	painter->save();
	const QRect rectReduit = rect.adjusted(1,1, -1,-1);
	//Si animé
	if(_frequence > 0.)
	{
		if (mode == QIcon::Disabled)
		{
			_stopTimer();
		}
		else
		{
			if( _idTimer == -1 )
			{
				_initTimer();
			}
			else
			{
				if( _typeAnimation == GFontIcon::AnimRotation )
				{
					//Calcul de l'angle en fonction du temps
					_angle = fmod( _angle + (_timeElapsed.restart() * _frequence / 1000.) * 360., 360.0);
					//Applique la transformation
					QTransform transform = painter->transform();
					//Vecteur de translation avec décallage de 1px
					const qreal x = qreal(rectReduit.width()) / 2. + 1.;
					const qreal y = qreal(rectReduit.height()) / 2. + 1.;
					transform.translate(x, y);
					transform.rotate(_angle);
					transform.translate(-x, -y);
					painter->setTransform(transform);
				}
				else
				{
					//Calcul de l'opacité en fonction du temps
					_opacity = fmod( _opacity + (_timeElapsed.restart() * _frequence / 1000.), 1.);
					painter->setOpacity(_opacity);
				}
			}
		}
    }
    //Dessin de l'icone
    GFontIconEngine::paint(painter, rect, mode, state);
    painter->restore();
}

QPixmap GFontAnimIconEngine::pixmap(const QSize &size, QIcon::Mode mode, QIcon::State state)
{
	QPixmap pix(size);
	pix.fill(Qt::transparent);
	//Dessine le pixmap
	QPainter painter(&pix);
	paint(&painter, QRect(QPoint(0,0),size), mode, state);
	return pix;
}

QIconEngine *GFontAnimIconEngine::clone() const
{
    return new GFontAnimIconEngine(_frequence, _letter, _baseColor, _typeAnimation, _font.family(), _parent);
}

void GFontAnimIconEngine::virtual_hook(int id, void *data)
{
    if( id == QIconEngine::ScaledPixmapHook )
    {
        ScaledPixmapArgument *info = reinterpret_cast<ScaledPixmapArgument*>(data);
        info->pixmap = pixmap(info->size, info->mode, info->state);
        return;
    }
    GFontIconEngine::virtual_hook(id, data);
}

void GFontAnimIconEngine::setFrequence(qreal newFrequence)
{
	_frequence = newFrequence;
}

void GFontAnimIconEngine::timerEvent(QTimerEvent *event)
{
	if( event->timerId() == _idTimer &&
		_parent)
	{
		//Repaint un bouton ou un menu
		if( !_repaintParentIcon(_parent))
		{
			//Repaint une action
			QAction *parentAction = qobject_cast<QAction*>(_parent);
			if(parentAction)
			{
				//Repaint chaque widget associé
				Q_FOREACH(QObject *parentObject, parentAction->associatedObjects())
				//Q_FOREACH(QWidget *parentWidget, parentAction->associatedWidgets())
				{
					//Repaint un bouton
					if( !_repaintParentIcon(parentObject, parentAction) )
					{
						 //qDebug() << "GFontAnimIconEngine::timerEvent" << _parent << parentAction << parentWidget;
					}
				}
			}
			else
			{
				//qDebug() << "GFontAnimIconEngine::timerEvent" << _parent;
			}
		}
	}

	QObject::timerEvent(event);
}




/**
 * private:
 */

void GFontAnimIconEngine::_initTimer()
{
	_idTimer = startTimer(40, Qt::PreciseTimer);
	_timeElapsed.start();
}

void GFontAnimIconEngine::_stopTimer()
{
	if( _idTimer > -1 )
	{
		killTimer(_idTimer);
		_idTimer = -1;
	}
	if( _timeElapsed.isValid() )
	{
		_timeElapsed.invalidate();
	}
}

bool GFontAnimIconEngine::_repaintParentIcon(QObject *parent, QAction *action)
{
	//Bouton
	QAbstractButton *parentButton = qobject_cast<QAbstractButton*>(parent);
	if(parentButton)
	{
		if(parentButton->isVisible())
		{
			parentButton->update();
		}
		return true;
	}
	//Label
	QLabel *parentLabel = qobject_cast<QLabel*>(parent);
	if(parentButton)
	{
		if(parentLabel->isVisible())
		{
			parentLabel->update();
		}
		return true;
	}
	//Menu
	QMenu *parentMenu = qobject_cast<QMenu*>(parent);
	if(parentMenu)
	{
		if(action)
		{
			if(parentMenu->isVisible())
			{
				parentMenu->repaint(parentMenu->actionGeometry(action));
			}
		}
		else
		{
			//qDebug() << "parentMenu" << _parent << parentMenu->isVisible() << parentMenu << action;
			parentMenu->update();
		}
		return true;
	}
    //qDebug() << "_repaintParentIcon: impossible de redessiner le parent" << _parent << parent << action;
	return false;
}
