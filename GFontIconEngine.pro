#-------------------------------------------------
#
# Project created by QtCreator 2017-01-03T13:37:08
#
#-------------------------------------------------
QT       -= gui
QT += core-private gui-private

TARGET = gfonticon

HEADERS += $$PWD/main.cpp \
    $$PWD/GFontIconEngine.h \
    $$PWD/GFontIconLibGlobal.h \
    $$PWD/gfonticonplugin.h

SOURCES += $$PWD/GFontIcon.cpp \
    $$PWD/GFontIconEngine.cpp \
    $$PWD/gfonticonplugin.cpp

#A voir
#PLUGIN_TYPE = iconengines
#PLUGIN_CLASS_NAME = GFontIconEnginePlugin
#QTC_PLUGIN_NAME = GFontIconEnginePlugin
#SOURCES += $$PWD/GFontIconEnginePlugin.cpp
#OTHER_FILES += gfonticonengine.json
