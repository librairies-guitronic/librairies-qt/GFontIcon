#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "GFontIcon.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QTabWidget>
#include <QStyle>
#include <QDebug>

#define TAILLE_ICON 25

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	//Version 6
	_addFamilyDemo(FONT_SOLID_5);
	ui->tabWidget->setTabIcon(0, GFontIcon::icon(0xf013, Qt::darkRed, FONT_SOLID_5) );
	//_addFamilyDemo(FONT_REGULAR_5);
	//ui->tabWidget->setTabIcon(1, GFontIcon::icon(0xf013, Qt::darkRed, FONT_REGULAR_5) );
	_addFamilyDemo(FONT_BRANDS_5);
	ui->tabWidget->setTabIcon(1, GFontIcon::icon(0xf013, Qt::darkRed, FONT_BRANDS_5) );
	//Version 6
	_addFamilyDemo(FONT_SOLID_6);
	ui->tabWidget->setTabIcon(2, GFontIcon::icon(0xf013, Qt::darkRed, FONT_SOLID_6) );
	//_addFamilyDemo(FONT_REGULAR_6);
	//ui->tabWidget->setTabIcon(4, GFontIcon::icon(0xf013, Qt::darkRed, FONT_REGULAR_6) );
	_addFamilyDemo(FONT_BRANDS_6);
	ui->tabWidget->setTabIcon(3, GFontIcon::icon(0xf013, Qt::darkRed, FONT_BRANDS_6) );

	//Test icones en menu
	ui->menuMenu->setIcon( GFontIcon::icon(0xf011, Qt::red, FONT_SOLID_5) );

	//Actions
	//ui->actiontest_icone_en_menu->setIcon( GIconAnimColor(0xf2f1, Qt::darkCyan, ui->actiontest_icone_en_menu) );
	ui->mainToolBar->addAction( ui->actiontest_icone_en_menu );
	ui->toolButton->setDefaultAction(ui->actiontest_icone_en_menu);


	//Test icone animé
	ui->pushButtonRefresh->setIcon( GFontIcon::iconAnimed(0xf4ad, ui->pushButtonRefresh, Qt::blue, GFontIcon::AnimClignote, FONT_SOLID_5, .5) );
	//ui->pushButtonRefresh->setIcon( GFontIcon::iconAnimed(0xf1ce, ui->pushButtonRefresh, Qt::blue, GFontIcon::AnimRotation, FONT_SOLID_5, .5) );
	ui->pushButtonRefresh_2->setIcon( GFontIcon::icon(0xf1ce, Qt::blue, FONT_SOLID_5) );
	ui->pushButtonRefresh_2->setIconSize(QSize(100,100));
	//ui->pushButtonRefresh_2->setIcon( GFontIcon::iconAnimed(0xf021, ui->pushButtonRefresh_2, Qt::darkBlue, GFontIcon::AnimRotation, FONT_SOLID_5, .5) );
	//ui->pushButtonRefresh_3->setIcon( GFontIcon::iconAnimed(0xf021, ui->pushButtonRefresh_3, Qt::darkGreen, GFontIcon::AnimRotation, FONT_REGULAR_6, .5) );
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::_addFamilyDemo(const QString &fontFamily)
{
	QWidget *widget = new QWidget(this);
	//Ajout du layout
	QVBoxLayout *layout = new QVBoxLayout(widget);
	widget->setLayout(layout);

	//TabWidget dans le widget
	QTabWidget *tab = new QTabWidget(widget);
	layout->addWidget(tab, 1);

	int code = 0xf000;
	for (int centaine = 0; centaine < 16; ++centaine)
	{
		QWidget *tabWidget = new QWidget(widget);
		tab->addTab(tabWidget, QString("0x%1").arg( QString::number(code, 16) ));

		QGridLayout *grid = new QGridLayout(tabWidget);
		tabWidget->setLayout(grid);
		for (int x=0; x < 16; ++x)
		{
			for (int y=0; y < 16; ++y, ++code)
			{
				QPushButton *button = new QPushButton(GFontIcon::icon(code, QColor(), fontFamily),
													  QString("0x%1").arg(QString::number(code, 16)), this);
				button->setIconSize( QSize(TAILLE_ICON, TAILLE_ICON) );
				grid->addWidget(button, x,y);
			}
		}
	}
	//Ajout au tabWidget
	ui->tabWidget->addTab(widget, GFontIcon::Family(fontFamily));
}
