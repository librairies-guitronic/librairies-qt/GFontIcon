#-------------------------------------------------
#
# Project created by QtCreator 2018-11-21T14:51:25
#
#-------------------------------------------------
QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = testFont
TEMPLATE = app
#Pour test
TARGET = testFont
CONFIG += c++20


# Insertion Lib
#include("$$PWD/../GFontIcon.pri")

# Insertion des fichiers
DEFINES += GFONTICON_INCLUDE
include("$$PWD/../GFontIconInclude.pri")

SOURCES += \
    main.cpp \
    MainWindow.cpp

HEADERS += \
    MainWindow.h

FORMS += \
    MainWindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
