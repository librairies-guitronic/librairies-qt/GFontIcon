#ifndef GFONTICONENGINE_H
#define GFONTICONENGINE_H

#include <QPainter>
#include <QIconEngine>
#include <QDebug>
#include "GFontIconLibGlobal.h"

/**
 * Classe de desssin des icones GFontIcon
 * @brief The GFontIconEngine class
 */
class GFONTICON_SHARED_EXPORT GFontIconEngine: public QIconEngine
{
public:
	/**
	 * Creation d'un pixmap de caractère à partir de la font
	 * @brief Pixmap
	 * @param code
	 * @param baseColor
	 * @param font
	 * @param size
	 * @param mode
	 * @param state
	 * @return
	 */
	static const QPixmap    Pixmap(const QChar &code, const QColor &baseColor, const QFont &font, const QSize &size, QIcon::Mode mode = QIcon::Active, QIcon::State state = QIcon::On);

	/**
	 * Dessin d'un caractère à partir de la font
	 * @brief Paint
	 * @param code
	 * @param baseColor
	 * @param font
	 * @param painter
	 * @param rect
	 * @param mode
	 * @param state
	 */
	static void             Paint(const QChar &code, const QColor &baseColor, const QFont &font, QPainter *painter, const QRect &rect, QIcon::Mode mode, QIcon::State state);

	explicit GFontIconEngine();
	GFontIconEngine(const QString &family, const QChar &letter, const QColor &baseColor = QColor());

	//Subclasse
	virtual void            paint(QPainter *painter, const QRect &rect, QIcon::Mode mode, QIcon::State state) Q_DECL_OVERRIDE;

	/**
	 * Fabrique et stocke le pixmap en cache
	 * @todo mettre en cache
	 * @brief pixmap
	 * @param size
	 * @param mode
	 * @param state
	 * @return
	 */
	virtual QPixmap         pixmap(const QSize &size, QIcon::Mode mode, QIcon::State state) Q_DECL_OVERRIDE;
	virtual QIconEngine*    clone() const Q_DECL_OVERRIDE;

    virtual QString         key() const Q_DECL_OVERRIDE;

    virtual QString         iconName()
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        const
#endif
        Q_DECL_OVERRIDE;

    virtual QList<QSize>    availableSizes(QIcon::Mode mode = QIcon::Normal, QIcon::State state = QIcon::Off)
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        const
#endif
        Q_DECL_OVERRIDE;

    /**
     * Prise en charge de l'échelle de l'Os
     * @brief virtual_hook
     * @param id
     * @param data
     */
	virtual void            virtual_hook(int id, void *data) Q_DECL_OVERRIDE;

protected:
	/**
	 * Parametres de dessin
	 * @brief _letter
	 */
	QChar       _letter;
	QColor      _baseColor;
	QFont       _font;
};

#endif // GFONTICONENGINE_H
