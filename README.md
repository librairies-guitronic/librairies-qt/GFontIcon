# Librairie d'intégration des fontawesome pour Qt

## La librairie intègre les fontawesome 

    - version 5
    - version 6 beta

### Compilation:
    Utiliser le fichier GFontIcon.pro

### Intégration:
    Ajouter le fichier pri à votre projet
        include("GFontIcon/GFontIcon.pri")

### Example d'utilisation:
    GIconColor(0xf2f9, Qt::red)
	
- Attention:
	Sous CentOs 7.5 les font awesome Solid et Regular ont le même nom, elle ne peuvent pas être utilisée simultanément.
    