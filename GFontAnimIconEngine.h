#ifndef GFONTANIMICONENGINE_H
#define GFONTANIMICONENGINE_H

#include "GFontIcon.h"
#include "GFontIconEngine.h"
#include <QAction>
#include <QElapsedTimer>
#include <QObject>

/**
 * @brief The GFontAnimIconEngine class
 */
class GFONTICON_SHARED_EXPORT GFontAnimIconEngine: public QObject, public GFontIconEngine
{
	Q_OBJECT

    public:
		GFontAnimIconEngine(QObject *parent);
		explicit GFontAnimIconEngine(const qreal &frequence, const QChar &letter, const QColor &baseColor, const GFontIcon::TypeAnimation &typeAnimation, const QString &family, QObject *parent);
		virtual ~GFontAnimIconEngine() Q_DECL_OVERRIDE;

		/**
		 * Fonctions subclasse
		 */
		void        paint(QPainter *painter, const QRect &rect, QIcon::Mode mode, QIcon::State state) Q_DECL_OVERRIDE;
		QPixmap     pixmap(const QSize &size, QIcon::Mode mode, QIcon::State state) Q_DECL_OVERRIDE;
		QIconEngine *clone() const Q_DECL_OVERRIDE;

        /**
         * Prise en charge de l'échelle de l'Os
         * @brief virtual_hook
         * @param id
         * @param data
         */
        void        virtual_hook(int id, void *data) Q_DECL_OVERRIDE;

		/**
		 * Définie la vitesse de l'animation
		 * @brief setFrequence
		 * @param newFrequence
		 */
        void        setFrequence(qreal newFrequence);

	protected:
		/**
		 * Intersepte le timer de dessin
		 * @brief timerEvent
		 * @param event
		 */
		void    timerEvent(QTimerEvent *event) Q_DECL_OVERRIDE;

	private:
		/**
		 * Type d'animation de l'icone
		 * @brief _typeAnimation
		 */
		const GFontIcon::TypeAnimation	_typeAnimation;

		/**
		 * Propriétés d'animation
		 * @brief _idTimer
		 */
		int     _idTimer = -1;

		/**
		 * Vitesse de rotation
		 *  en tour / seconde
		 * @brief _frequence
		 */
		qreal   _frequence = 1.;

		/**
		 * Angle en cours
		 *	- Utile pour AnimRotation
		 * @brief _angle
		 */
		qreal   _angle = 0.;

		/**
		 * Opacité en cours
		 *	- Utile pour AnimClignote
		 * @brief _opacity
		 */
		qreal   _opacity = 0.;

		/**
		 * Temps entre 2 dessins
		 * @brief _timeElapsed
		 */
		QElapsedTimer   _timeElapsed;

		/**
		 * Pointeur du parent pour mise à jour
		 * @brief _parent
		 */
		QObject *_parent;

		/**
		 * Initialise le timer
		 * @brief _initTimer / _stopTimer
		 */
		void    _initTimer();
		void    _stopTimer();

		/**
		 * Update le parent de l'icon
		 *  Renvois true si le parent correcpondant a été trouvé
		 * @brief _repaintParentIcon
		 * @param parent
		 * @return
		 */
		bool    _repaintParentIcon(QObject *parent, QAction *action = Q_NULLPTR);
};

#endif // GFONTANIMICONENGINE_H
