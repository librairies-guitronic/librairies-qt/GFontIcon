#-------------------------------------------------
#
# Fichier pri pour insertion Dll GFontIcon
#
#-------------------------------------------------
# Information programme à rajouter dans le fichier pri d'origine
LIB_NAME = GFontIcon
LIB_VERSION = 1
#LIB_VERSION = 1.0.4
LIB_PWD = $$PWD

# Complete informations Guitronic par default
#exists( $$PWD/../GlobalIncludeGuitronic.pri ) {
#    include("$$PWD/../GlobalIncludeGuitronic.pri")
#}
#else {
    # Ajout de la version de Qt au nom du fichier
    LIB_NAME = $$join(LIB_NAME,,,_$${QT_VERSION})
	
    static {
        message("Compilation STATIC de la librairie Guitronic: $$TARGET")
        DEFINES += GFONTICON_INCLUDE
    }
    win32::LIB_NAME = $$join(LIB_NAME,,,_$${LIB_VERSION})

    # Inclusion de la librairie
    INCLUDEPATH += $$LIB_PWD
    message("Inclusion librairie Guitronic: $$LIB_PWD/lib/$$LIB_NAME")
    LIBS += -L"$$LIB_PWD/lib" -l$$LIB_NAME
#}
